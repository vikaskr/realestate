(function($, document, window){
	
	$(document).ready(function(){

		// Cloning main navigation for mobile menu
		$(".mobile-navigation").append($(".main-navigation .menu").clone());

		// Mobile menu toggle 
		$(".menu-toggle").click(function(){
			$(".mobile-navigation").slideToggle();
		});
	});

	$(window).load(function(){
		$(".feature-slider").flexslider({
			directionNav: true,
			controlNav: false,
			prevText: '<i class="fa fa-angle-left"></i>',
			nextText: '<i class="fa fa-angle-right"></i>',
		});
	});

})(jQuery, document, window);
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters"); 

$('#contactForm').validate({	
		errorClass: "has-error",
		validClass: "has-success",

		rules: {

			name: {
				required: true,
				lettersonly: true,
				minlength: 4
			},

			email: {
				required: true,
				email: true
			},
			mobile: {
				required: true,
        		number: true
			},
			city: {
				required: true,
			},
		
			messages: {
			name: "Please enter your name",
    		email: "Please enter a valid email address",
    		mobile	: "please enter a valid mobile number",
    		city	: "please enter your city"
		}
	},
	submitHandler : function(form) {console.log('contactForm');
    var form = $('#contactForm').serialize();
		$.ajax({
         type:'POST',
         url:"db.php",
         data:form,
        success: function(result)
        {
        	alert('Thanks for contacting us! We will call you soon');
            window.location.reload();
        }
    	});
	}
});
