<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<meta name="description" content="Raison d'être Real Estate developer with a wide range of commercial and residential properties in Mumbai.">
        <meta name="keywords" content="lodha eternis, 2 bhk flat andheri, 3 bhk flat andheri, buy property in Mumbai, Residential property in wadala"> 
		<title>Raison d'être Real Estate</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="style.css">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->
		<style type="text/css">
		.xs-hidden{
			display: none!important;
		}
		</style>

	</head>


	<body>
		
		<div class="site-content">
			<header class="site-header" data-bg-image="images/banner.jpg">
				<div class="container">
					<a href="index.php" class="branding">
						<img src="images/logo.png" alt="Apartments"><br/>
						<img src="images/img.png" alt="Apartments" width="200">
					</a>
				</div>
				<img src="images/newww.png" class="xs-hidden" alt="Apartments" width="700" style="margin-top: 30em; margin-left: 8em;">
				<div class="banner" >
					<div class="banner-content">
						<div class="container">
							<div class="cta" id="top">
								<a href="#">
									<a href="#" class="arrow-button"><i class="fa fa-angle-right"></i></a>
									<h2>Quick Enquiry</h2>
									<small>THE HOTTEST HUB WITH EXCELLENT CONNECTIVITY</small>
								</a>
							</div>

							<div class="subscribe-form">
								<form name="frm" id="contactForm">
									<small class="form-subtitle">Start to live in Your</small>
									<h2 class="form-title">New Apartment</h2>

									<div class="control">
										<input type="text" name="name" placeholder="Name" >
										<i class="fa fa-user"></i>
									</div>
									<div class="control">
										<input type="email" name="email" placeholder="Email">
										<i class="fa fa-envelope"></i>
									</div>
									<div class="control">
										<input type="text" name="mobile" placeholder="Mobile number">
										<i class="fa fa-phone"></i>
									</div>
									<div class="control">
										<select name="city">
											  <option value="mumbai">Mumbai</option>
											  <option value="navi-mumbai">Navi-Mumbai</option>
											  <option value="waldal">Wadala</option>
										</select>
										
										<i class="fa fa-map-marker"></i>
									</div>
									<input type="submit" value="Submit">
								</form>
							</div>

						</div>
					</div>
				</div>
			</header> <!-- .site-header --><br><br>
			<div class="container pading"><br>
				<div class="container">
					<h1 style="color: #d9c08a;">Save up to ₹ 70 Lacs. Last pre-launch before the BKC-Sion elevated road opens.</h1 style="color: #d9c08a;">
					<p style="font-size: 17px;">
Pre-launching Lodha Codename SmartMove - The Last pre-launch by Lodha Group at New Cuffe Parade before BKC-Sion Elevated Road opens in Dec 2018 making commute to BKC just a 10 minute affair.  This will likely move the prices here to a level comparable to new developments in Bandra’s MIG Colony (~2x of current prices at New Cuffe Parade).
<br/><br/>
The 38 storey skyscraper features best-selling 1 bed, 2 bed & Private garden Residences and is also one of the best located towers of the development being close to the Clubhouse, Retail and Commercial Tower. The development features India's largest luxury clubhouse at 75,000 sq.ft., 15 acres of open spaces, iconic office tower and a world class ICSE School - The Shri Ram Universal School. With the availability of the choicest inventory at pre-launch prices and savings up to 70 Lakhs*, it's time you made the smart move.</p>
						
						</div>

				<div class="row">

					<div class="col-md-6"><h3 class="section-title">About Project</h2><p>This Projects is the last pre-launch at NCP before the BKC-Sion connector opens. This connector once operational in Dec’18, will reduce commute time to BKC to just 10 mins. This makes NCP just as valuable as MIG Colony / Kalanagar which are transacting at 2X value. The latest skyscraper at New Cuffe Parade is launched with the backdrop of several significant milestones for New Cuffe Parade. With Phase-1 delivery to over 500 families, Alliance with India’s leading ICSE school – The Shri Ram Universal School and Commercial Tower occupancy in the pipeline, the mood at New Cuffe Parade is at an all -time high. And with this pre-launch we are bringing back the best-selling 1 Bed, 2 Bed & Private Garden residences at the one of the best located tower at NCP at pre-launch prices and optimal payment schedule.</p></div>
					<div class="col-md-6"><h3 class="section-title">The key levers for sourcing are:</h3>
					<p class="new">Last pre-launch before the BKC-Sion corridor opens<br/></p>
					<p class="new">Pre-launch prices for first 45 bookings <br/></p>
				    <p class="new">Best-selling inventory – 1 Bed, 2 Bed Smart & Garden residences are back<br/></p>
				    <p class="new">Optimal payment schedule<br/></p>
					</div>
				</div>
			</div>
			<h3 class="section-title" style="text-align: center;">Why Invest in this Project</h2>
			<main class="main-content">
				<div class="container"><div class="post-list">
							<article class="post heigh">
								<figure class="feature-image" data-bg-image="images/realstate.png" height="417"></figure>
								<div class="post-detail">
									<h4 class="entry-title">1200+ Cr Revenue for Harbor Market</h4>
									<h4 class="entry-title">850+ Cr Revenue for New Cuffe Parade</h4>
									<h4 class="entry-title">Wadala 2.0 – The next business district</h4>
									<h4 class="entry-title">On the lines of BKC, Nariman Point</h4>
									<h4 class="entry-title">64 Hectares of Land</h4>
									<h4 class="entry-title">WiFi, CCTV and Smart Parking</h4>
									<h4 class="entry-title">Residential and commercial</h4>
									<div class="hidden-md hidden-lg hidden-sm">
										<a href="#" class="button" style="margin-top: 5em;">	Request a Callback</a>
									</div>
								</div>
							</article>
							
						</div>	
					</div>	
				</div><br><br>
				<div class="fullwidth-block" data-bg-color="#f0f0f0">
					<div class="container">
						<h2 class="section-title">What is so great in our apartments?</h2>
						<div class="row">
							<div class="col-md-12">
								<img src="images/new.jpg" width="99%">
							</div>
							
						</div>
					</div>
				</div><br><br>
				<div class="fullwidth-block pad">
					<div class="container" style="margin-bottom: -3em!important; text-align: center;">
						<h1>Office Address</h1>
						<h3>428, 4th floor, Ashapura co-op housing society Kala Nagar ,Bandra East Mumbai 51,  Maharastra.</h3>
					</div>
				</div>
			</main>

			<footer class="site-footer">
				<div class="container"><br>
					    <a href="index.php" class="new3">Home</i></a>
						<a href="about.php" target="_blank" class="new3">About this Project</a>
						<a href="privacy.html" class="new3" target="_blank">Privacy & Policy</a>
						<a href="term.html" target="_blank" class="new3">Term & Conditions</a>

						<a href="#" class="new3">Contact us</a>
				</div>
			</footer>
		</div>
<button onclick="topFunction()" id="myBtn" title="Go to top">Request Callback</button>
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>

</html>