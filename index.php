<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		<meta name="description" content="Raison d'être Real Estate developer with a wide range of commercial and residential properties in Mumbai.">
        <meta name="keywords" content="lodha eternis, 2 bhk flat andheri, 3 bhk flat andheri, buy property in Mumbai, Residential property in wadala"> 
		<title>Raison d'être Real Estate</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="style.css">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->
		<style type="text/css">
		.xs-hidden{
			display: none!important;
		}
		</style>

	</head>


	<body>
		
		<div class="site-content">
			<header class="site-header" data-bg-image="images/banner.jpg">
				<div class="container">
					<a href="#" class="branding">
						<img src="images/logo.png" alt="Apartments"><br/>
						<img src="images/img.png" alt="Apartments" width="200">
						
					</a>
				</div>
				<img src="images/newww.png" class="xs-hidden" alt="Apartments" width="700" style="margin-top: 30em; margin-left: 8em;">
				<div class="banner" >
					<div class="banner-content">
						<div class="container" style="height: 0px;">
							<div class="cta" id="top">
								<a href="#">
									<a href="#" class="arrow-button"><i class="fa fa-angle-right"></i></a>
									<h2>Quick Enquiry</h2>
									<small>THE HOTTEST HUB WITH EXCELLENT CONNECTIVITY</small>
								</a>
							</div>

							<div class="subscribe-form">
								<form name="frm" id="contactForm">
									<small class="form-subtitle">Start to live in Your</small>
									<h2 class="form-title">New Apartment</h2>

									<div class="control">
										<input type="text" name="name" placeholder="Name" >
										<i class="fa fa-user"></i>
									</div>
									<div class="control">
										<input type="email" name="email" placeholder="Email">
										<i class="fa fa-envelope"></i>
									</div>
									<div class="control">
										<input type="text" name="mobile" placeholder="Mobile number">
										<i class="fa fa-phone"></i>
									</div>
									<div class="control">
										<select name="city">
											  <option value="mumbai">Mumbai</option>
											  <option value="navi-mumbai">Navi-Mumbai</option>
											  <option value="waldal">Wadala</option>
										</select>
										
										<i class="fa fa-map-marker"></i>
									</div>
									<input type="submit" value="Submit">
								</form>
							</div>
						</div>
					</div>
				</div>
			</header> <!-- .site-header --><br><br>
			<div class="container">
				<div class="row">
					<div class="col-md-6"><h2 class="new2">MUMBAI’S MOST WELL-CONNECTED LOCATION POISED FOR EXPONENTIAL GROWTH.</h2><h2 class="new2">LAST OPPORTUNITY TO BUY IN A PRE-LAUNCH AT NCP, BEFORE BKC-SION CONNECTOR OPENS.</h2></div>
					<div class="col-md-6"><p class="new">PRE-LAUNCH OFFER - SAVE UPTO ?70 LACS.<br/></p>
					<p class="new">PRE-LAUNCH OFFER FOR FIRST 45 RESIDENCES ONLY <br/></p>
				    <p class="new">1 Bed: ₹ 1.62 cr+ | 2 Bed: ₹ 2.09 cr+ | 3 Bed Luxe: ₹ 3.99 cr+<br/></p>
					</div>
				</div>
			</div>
			<main class="main-content">
				<div class="fullwidth-block" data-bg-color="#ffffff">
					<div class="container">
					<h1 style="color: #d9c08a;">Save up to ₹ 70 Lacs. Last pre-launch before the BKC-Sion elevated road opens.</h1 style="color: #d9c08a;">
					<p style="font-size: 17px;">
Pre-launching Lodha Codename SmartMove - The Last pre-launch by Lodha Group at New Cuffe Parade before BKC-Sion Elevated Road opens in Dec 2018 making commute to BKC just a 10 minute affair.  This will likely move the prices here to a level comparable to new developments in Bandra’s MIG Colony (~2x of current prices at New Cuffe Parade).
<br/><br/>
The 38 storey skyscraper features best-selling 1 bed, 2 bed & Private garden Residences and is also one of the best located towers of the development being close to the Clubhouse, Retail and Commercial Tower. The development features India's largest luxury clubhouse at 75,000 sq.ft., 15 acres of open spaces, iconic office tower and a world class ICSE School - The Shri Ram Universal School. With the availability of the choicest inventory at pre-launch prices and savings up to 70 Lakhs*, it's time you made the smart move.</p>
						<div class="row"><div class="col-md-3"></div><div class="col-md-6"><p class="new" style="font-size: 18px!important;">Prelaunch offer on first 45 residences only</p></div></div>
						</div>

						
						<div class="post-list">
							<article class="post">
								<figure class="feature-image" data-bg-image="images/realstate.png" height="417"></figure>
								<div class="post-detail">
									<h2 class="entry-title">THE MOST AMAZING LIFESTYLE THAT MUMBAI CITY CAN OFFER</h2>
									<p>Codename SmartMove is a grand 38 storey tower with world-class 1,2 and 3 bed residences and select residences with sky gardens. The apartments have 3 sides open and each room has a sun deck to enioy the beautiful views.

							<div class="row">
								<div class="col-md-6">
								<h2 class="entry-title">Best-in-class residences</h2>

									<ul><li>Fully air-conditioned residences“</li>

									<li>Italian marble flooring in living & dining room</li>

									<li>Marbital® flooring in bedrooms</li>

									<li>European designed bath fittings from Duravit</li>
									and Grohe’v‘

									<li>Modular kitchen with hob, chimney and sink</li>

									<li>Select private garden residences</li>
									</ul>
								</div>

								<div class="col-md-6">
									<h2 class="entry-title">Phase-1 delivered</h2>

									<ul><li>4 towers ready for possession — over 1000 units
									now ready</li>

									<li>Phase-1 of the clubhouse operational</li>

									<li>Satellite clubhouse open for use</li>

									<li>Phase-1 grand entrance fully functional</li>
									</ul>
									<br/><br/>
											<a href="#" class="button">	Request a Callback</a>
										</div>
									</div>
							
								</div>
							</article>
							<article class="post hei">
								<figure class="feature-image" data-bg-image="images/realstate2.png"></figure>
								<div class="post-detail"><h2 class="entry-title">WORLD-CLASS HOMES IN AN EXCLUSIVE TOWER</h2>
								
								<h2 class="entry-title">75,000 sq. ft. Luxury Clubhouse</h2>
								<p>Not iust India’s largest luxury clubhouse but a whole new standard of international living. Featuring everything from a poolside cafe to a cinema, your social life will definitely become busier. With Phase-I of the clubhouse already operational, come see it to believe it.</p>

								<h2 class="entry-title">15 acres of Sheer bliss</h2>
								<p>3 times as large as Wankhede Ground - 15 acres of landscapes and open spaces with thousands of trees and every sports amenity that you can dream of — 11 swimming pools, cricket field, tennis court, basketball court, kids’ play area and much more.</p>

								<div class="hidden-xs"><h2 class="entry-title">ICSE School</h2>
								<p>With India's leading ICSE School - The Shri Ram Universal School in the development, your child can hop, skip and iump to school.</p></div>

								<h2 class="entry-title">Ganesha and Jain Temple</h2>
								<p>For the divine connect, we have thoughtfully integrated Ganesha Temple and Jain Temple within the development. Feel the sense of spirituality, devotion and peace in these holy places ofworship.</p>

								<h2 class="entry-title">F&B and Retail outlets</h2>
								<p>Your favourite restaurants as well as convenience shopping — including retail supermarket and more — within your development. Never step out.</p><br/>
											<a href="#" class="button">	Request a Callback</a>

								</div>

							</article>
						</div>	
					</div>	
				</div>

				<div class="fullwidth-block" data-bg-color="#f0f0f0">
					<div class="container">
						<h2 class="section-title">What is so great in our apartments?</h2>
						<div class="row">
							<div class="col-md-12">
								<img src="images/new.jpg" width="99%">
							</div>
							
						</div>
					</div>
				</div><br><br>
				<div class="fullwidth-block pad">
					<div class="container" style="margin-bottom: -3em!important; text-align: center;">
						<h1>Office Address</h1>
						<h3>428, 4th floor, Ashapura co-op housing society Kala Nagar ,Bandra East Mumbai 51,  Maharastra.</h3>
					</div>
				</div>
			</main>

			<footer class="site-footer">
				<div class="container"><br>
					    <a href="#" class="new3">Home</i></a>

						<a href="about.php" target="_blank" class="new3">About this Project</a>
						<a href="privacy.html " class="new3" target="_blank">Privacy & Policy</a>
						<a href="term.html" target="_blank" class="new3">Term & Conditions</a>

						<a href="#" class="new3">Contact us</a>
				</div>
			</footer>
		</div>
		<button onclick="topFunction()" id="myBtn" title="Go to top">Request Callback</button>
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
		<script src="js/app.js"></script>
		
		
	</body>

</html>